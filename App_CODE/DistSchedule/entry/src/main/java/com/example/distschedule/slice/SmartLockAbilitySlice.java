/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.distschedule.slice;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.distschedule.ResourceTable;
import com.example.distschedule.bean.Command;
import com.example.distschedule.bean.DeviceResult;
import com.example.distschedule.bean.RespondBase;
import com.example.distschedule.bean.UserShareResult;
import com.example.distschedule.rx2.OKHttpUtilsRx2;
import com.example.distschedule.toast.XToastUtils;
import com.example.distschedule.utils.CommandUtil;
import com.example.distschedule.utils.DeviceStateUtil;
import com.example.distschedule.utils.PreferencesUtil;
import com.example.distschedule.utils.Util;
import com.socks.library.KLog;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.miscservices.timeutility.Time;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static ohos.agp.components.InputAttribute.PATTERN_NUMBER;
import static ohos.agp.components.InputAttribute.PATTERN_PASSWORD;


/**
 * 烟感控制页面
 *
 *
 */
public class SmartLockAbilitySlice extends AbilitySlice {
    private static final String KEY_CHANGE_MASTER_CODE = "change_master_password";
    private static final String KEY_CHANGE_TEMP_CODE = "change_temp_password";
    private boolean isLockON = false;
    private boolean isTempON = false;

    private boolean isShowMaster = false;
    private boolean isShowTemp = false;

    private boolean isLockAlertON = false;
    private boolean isPskResetON = false;

    private boolean isFingerprintON = false;
    private boolean isIntercomON = false;

    private DirectionalLayout tempPasswordShow;

    private Image logoLockImg;
    private Image switchImg;
    private Image fingerprintImg;
    private Image intercomImg;

    private Image showMasterPassword;
    private Image showTempPassword;

    private Image addTempPasswordSwitch;
    private Button addTempPasswordSubmit;

    private Text lockTime;
    private Text lockSwitchState;
    private Text alertText;

    private String deviceId;
    private String lastCode = "888888";

    private Text masterPwd;
    private Text tempPwd;

    private Text startTimeText;
    private Text endTimeText;


    private int startTimeToMinute = 960; // 默认16点 是 960分钟 设备端只认分钟
    private int endTimeToMinute = 1080; // 默认18点 是 1080 分钟

    private String userPhone; //用户手机号 11位
    private int isTemporaryUser = 0; // 0 不是临时用户，1 是临时用户

    private static final int EVENT_MSG_GET = 0x1000001;
    private Timer getOpenLockResult = null;
    private TimerTask getOpenLockResultTask = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_smart_lock);

        deviceId = intent.getStringParam("deviceId");

        userPhone = PreferencesUtil.getUserPhone(getContext());
        KLog.i("userPhone====" + userPhone + "deviceId==" + deviceId);
        initComponents();
        initDeviceState();
    }

    private void initLockDetectorState() {
        TaskDispatcher globalTaskDispatcher = getGlobalTaskDispatcher(TaskPriority.DEFAULT);
        globalTaskDispatcher.asyncDispatch(() -> {
            KLog.i("async task initLampState run");
            checkDeviceNet();
        });
    }

    private void initDeviceState() {
        OKHttpUtilsRx2.INSTANCE.getApi().getDeviceInfo(deviceId, PreferencesUtil.getUserId(getContext()))
                .compose(OKHttpUtilsRx2.INSTANCE.ioMain())
                .subscribe(new OKHttpUtilsRx2.SubjectImpl<RespondBase<DeviceResult>>() {
                    @Override
                    public void onNext(RespondBase<DeviceResult> deviceInfo) {
                        KLog.i(deviceInfo);
                        if (deviceInfo.isSuccess() && deviceInfo.getResult().isOnline()) {
                            // 更新状态
                            String property = deviceInfo.getResult().getProperty();
                            isLockAlertON = DeviceStateUtil.getKeyValue(property, "LockAbnormal").equals("1");
                            isPskResetON = DeviceStateUtil.getKeyValue(property, "OriginalPsk").equals("1");
                            if(isLockAlertON || isPskResetON) {
                                goToAlert();
                            }

                        } else {
                            initLockDetectorState();
                        }
                        if (!deviceInfo.getResult().getOwnerPhone().matches(userPhone)) {
                            //不是设备拥有者，继续查询是不是被分享者，如果都不是就是临时用户
                            getShareResult();
                        }
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        initLockDetectorState();
                    }
                });
    }

    private void getOpenLockResult() {
        OKHttpUtilsRx2.INSTANCE.getApi().getDeviceInfo(deviceId, PreferencesUtil.getUserId(getContext()))
                .compose(OKHttpUtilsRx2.INSTANCE.ioMain())
                .subscribe(new OKHttpUtilsRx2.SubjectImpl<RespondBase<DeviceResult>>() {
                    @Override
                    public void onNext(RespondBase<DeviceResult> deviceInfo) {
                        KLog.i(deviceInfo);
                        if (deviceInfo.isSuccess() && deviceInfo.getResult().isOnline()) {
                            // 更新状态
                            String property = deviceInfo.getResult().getProperty();
                            String lockState = DeviceStateUtil.getKeyValue(property, "LockAbnormal");
                            KLog.i("=====lockState==" +lockState);
                            if (lockState.equals("2")) {
                                //提示开锁输入密码错误
                                XToastUtils.warning("密码错误，开锁失败");
                            }
                            else if(lockState.equals("8")){
                                lockSwitchState.setText("门已开锁");
                                lockTime.setText("00:00:10");
                                lockTime.setTextColor(Color.BLACK);
                                switchImg.setPixelMap(ResourceTable.Media_icon_switch_on);
                                isLockON = true;
                            }else if(lockState.equals("9"))
                            {
                                lockSwitchState.setText("门已关锁");
                                switchImg.setPixelMap(ResourceTable.Media_icon_switch_off);
                                isLockON = false;
                            }
                        }
                    }
                });
    }

    private void getShareResult() {
        OKHttpUtilsRx2.INSTANCE.getApi().getUserShare(deviceId)
                .compose(OKHttpUtilsRx2.INSTANCE.ioMain())
                .subscribe(new OKHttpUtilsRx2.SubjectImpl<RespondBase<List<UserShareResult>>>() {
                    @Override
                    public void onNext(RespondBase<List<UserShareResult>> respond) {
                        super.onNext(respond);
                        if (respond.isSuccess()) {
                            shareSuccess(respond);
                        }
                    }
                });
    }

    private void shareSuccess(RespondBase<List<UserShareResult>> respond) {
        if (respond.getResult() == null || respond.getResult().size() == 0) {
            //没有被分享的用户，就是临时用户
            isTemporaryUser = 1;
        } else {
            int index;
            for (index = 0; index < respond.getResult().size(); index++) {
                if (respond.getResult().get(index).getPhone().matches(userPhone)) {
                    isTemporaryUser = 0;
                    //被分享的用户，不是临时用户
                    break;
                }
            }
            if (index == respond.getResult().size()) {
                //是临时用户，不是被分享的用户
                isTemporaryUser = 1;
            }
        }
    }

    private void checkDeviceNet() {
        Time.sleep(10 * 1000);
        OKHttpUtilsRx2.INSTANCE.getApi().getDeviceInfo(deviceId, PreferencesUtil.getUserId(getContext()))
                .compose(OKHttpUtilsRx2.INSTANCE.ioMain())
                .subscribe(new OKHttpUtilsRx2.SubjectImpl<RespondBase<DeviceResult>>() {
                    @Override
                    public void onNext(RespondBase<DeviceResult> deviceInfo) {
                        super.onNext(deviceInfo);
                        KLog.i(deviceInfo);
                        if (deviceInfo.getResult() == null || !deviceInfo.getResult().isOnline()) {
                            XToastUtils.toast("未配网成功，请再次碰一碰重试");
                        }
                    }
                });
    }

    private void initComponents() {
        logoLockImg = (Image) findComponentById(ResourceTable.Id_lock_image_logo);
        lockTime = (Text) findComponentById(ResourceTable.Id_lock_text_time);
        lockSwitchState = (Text) findComponentById(ResourceTable.Id_lock_text_switch);
        lockSwitchState.setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
        lockSwitchState.setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);

        alertText = (Text) findComponentById(ResourceTable.Id_lock_alerting);

        findComponentById(ResourceTable.Id_lock_img_left).setClickedListener(this::goBack);
        findComponentById(ResourceTable.Id_lock_img_right).setClickedListener(this::goInfo);

        switchImg = (Image) findComponentById(ResourceTable.Id_lock_image_switch);
        switchImg.setClickedListener(component -> openLock());

        showMasterPassword = (Image) findComponentById(ResourceTable.Id_master_password_image);
        showMasterPassword.setClickedListener(component -> displayMasterPassword());
        showTempPassword = (Image) findComponentById(ResourceTable.Id_temporary_password_image);
        showTempPassword.setClickedListener(component -> displayTempPassword());

        fingerprintImg = (Image) findComponentById(ResourceTable.Id_img_fingerprint);
        fingerprintImg.setClickedListener(component -> fingerprintSelect());
        intercomImg = (Image) findComponentById(ResourceTable.Id_img_intercom);
        intercomImg.setClickedListener(component -> intercomSelect());


        addTempPasswordSwitch = (Image) findComponentById(ResourceTable.Id_temporary_password_switch);
        addTempPasswordSwitch.setClickedListener(component -> openEditTempSwitch());
        tempPasswordShow = (DirectionalLayout) findComponentById(ResourceTable.Id_temp);
        addTempPasswordSubmit = (Button) findComponentById(ResourceTable.Id_temporary_password_submit);
        addTempPasswordSubmit.setClickedListener(component -> submitTempPassword());


        Image editMasterPassword = (Image) findComponentById(ResourceTable.Id_master_password_edit);
        editMasterPassword.setClickedListener(component -> changeMasterPassword());

        Image editTempPassword = (Image) findComponentById(ResourceTable.Id_temporary_password_edit);
        editTempPassword.setClickedListener(component -> changeTempPassword());


        findComponentById(ResourceTable.Id_start_time).setClickedListener(component -> createTimePicker((Text) component, "start"));
        findComponentById(ResourceTable.Id_end_time).setClickedListener(component -> createTimePicker((Text) component, "end"));

        masterPwd = (Text) findComponentById(ResourceTable.Id_master_password);
        tempPwd = (Text) findComponentById(ResourceTable.Id_temporary_password);
        if (PreferencesUtil.getString(getContext(), KEY_CHANGE_MASTER_CODE) != null) {
            masterPwd.setText(PreferencesUtil.getString(getContext(), KEY_CHANGE_MASTER_CODE));
        }
        if (PreferencesUtil.getString(getContext(), KEY_CHANGE_TEMP_CODE) != null) {
            tempPwd.setText(PreferencesUtil.getString(getContext(), KEY_CHANGE_TEMP_CODE));
        }

        startTimeText = (Text) findComponentById(ResourceTable.Id_start_time_text);
        endTimeText = (Text) findComponentById(ResourceTable.Id_end_time_text);
    }

    private void openLockDialog() {
        CommonDialog commonDialog = new CommonDialog(getContext());
        commonDialog.setAlignment(LayoutAlignment.BOTTOM);
        commonDialog.setSize(Util.vp2px(getContext(), 336), Util.vp2px(getContext(), 250));
        commonDialog.setCornerRadius(Util.vp2px(getContext(), 24));
        Component container = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_open_lock,
                null, false);
        TextField password = (TextField) container.findComponentById(ResourceTable.Id_open_lock_password);
        password.setText(lastCode);
        commonDialog.setContentCustomComponent(container);
        commonDialog.setAutoClosable(true);
        commonDialog.show();
        addOpenLockListener(container, commonDialog);
    }

    private void addOpenLockListener(Component component, CommonDialog Dialog) {
        Button cancelImg = (Button) component.findComponentById(ResourceTable.Id_open_lock_cancel);
        Button okImg = (Button) component.findComponentById(ResourceTable.Id_open_lock_save);
        TextField password = (TextField) component.findComponentById(ResourceTable.Id_open_lock_password);

        cancelImg.setClickedListener(component1 -> Dialog.destroy());
        okImg.setClickedListener(component12 -> {

            lastCode = password.getText();
            //只发开锁
            sendCommand(CommandUtil.getLockStatusCommand(true, isTemporaryUser, password.getText()));
            if (getOpenLockResult == null && getOpenLockResultTask == null) {
                getOpenLockResult = new Timer();
                getOpenLockResultTask = new TimerTask() {
                    public void run() {
                        handler.sendEvent(EVENT_MSG_GET);
                    }
                };
                getOpenLockResult.schedule(getOpenLockResultTask, 0, 500);
            }
            Dialog.destroy();

        });
    }

    private void openLock() {
        if (isLockAlertON || isPskResetON) {
            //回到正常控制页面
            logoLockImg.setPixelMap(ResourceTable.Media_icon_lock);
            lockSwitchState.setText("门已关锁");
            lockTime.setText("");
            switchImg.setPixelMap(ResourceTable.Media_icon_switch_off);
            alertText.setVisibility(Component.HIDE);
            isLockAlertON = false;
            isPskResetON = false;
        } else {
            if (!isLockON) {
                openLockDialog();
            }else {
                XToastUtils.info("门已开锁，请关锁后再开锁");
            }
        }
    }

    private final EventHandler handler = new EventHandler(EventRunner.current()) {
        @Override
        protected void processEvent(InnerEvent event) {
            if (event.eventId == EVENT_MSG_GET) {
                getUITaskDispatcher().delayDispatch(() -> {
                    getOpenLockResult();
                    KLog.i("===========getOpenLockResult====");
                }, 0);
            }
        }
    };

    //显示主密码
    private void displayMasterPassword() {
        isShowMaster = !isShowMaster;
        if (isShowMaster) {
            //临时用户不可以查看密码
            if (isTemporaryUser == 0) {
                showMasterPassword.setPixelMap(ResourceTable.Media_icon_password_display);
                masterPwd.setTextInputType(PATTERN_NUMBER);
            } else {
                XToastUtils.warning("临时用户不能查看密码");
            }
        } else {
            //临时用户不可以查看密码
            if (isTemporaryUser == 0) {
                showMasterPassword.setPixelMap(ResourceTable.Media_icon_password_hide);
                masterPwd.setTextInputType(PATTERN_PASSWORD);
            } else {
                XToastUtils.warning("临时用户不能查看密码");
            }
        }
    }

    //显示临时密码
    private void displayTempPassword() {
        isShowTemp = !isShowTemp;
        if (isShowTemp) {
            //临时用户不可以查看密码
            if (isTemporaryUser == 0) {
                showTempPassword.setPixelMap(ResourceTable.Media_icon_password_display);
                tempPwd.setTextInputType(PATTERN_NUMBER);
            } else {
                XToastUtils.warning("临时用户不能查看密码");
            }
        } else {
            //临时用户不可以查看密码
            if (isTemporaryUser == 0) {
                showTempPassword.setPixelMap(ResourceTable.Media_icon_password_hide);
                tempPwd.setTextInputType(PATTERN_PASSWORD);
            } else {
                XToastUtils.warning("临时用户不能查看密码");
            }
        }
    }

    //切换指纹图片
    private void fingerprintSelect() {
        isFingerprintON = !isFingerprintON;
        if (!isFingerprintON) {
            //切换选中图片
            fingerprintImg.setPixelMap(ResourceTable.Media_icon_fingerprint);
            XToastUtils.info("功能暂未实现，敬请期待");
        } else {
            fingerprintImg.setPixelMap(ResourceTable.Media_icon_fingerprint_unselect);
        }
    }

    //切换对讲图片
    private void intercomSelect() {
        isIntercomON = !isIntercomON;
        if (!isIntercomON) {
            //切换选中图片
            intercomImg.setPixelMap(ResourceTable.Media_icon_intercom);
            XToastUtils.info("功能暂未实现，敬请期待");
        } else {
            intercomImg.setPixelMap(ResourceTable.Media_icon_intercom_unselect);
        }
    }


    //切换告警页面
    private void goToAlert() {
        logoLockImg.setPixelMap(ResourceTable.Media_icon_device_alert);
        alertText.setVisibility(Component.VISIBLE);
        lockTime.setText("00:00:10");
        lockTime.setTextColor(Color.RED);

        if (isPskResetON && isLockAlertON) {
            lockSwitchState.setText("密码重置且门未上锁");
            masterPwd.setText("888888");
            tempPwd.setText("");
            lockSwitchState.startAutoScrolling();
        } else if (isPskResetON) {
            lockSwitchState.setText("密码重置");
            masterPwd.setText("888888");
            tempPwd.setText("");
            lockSwitchState.startAutoScrolling();
        } else if (isLockAlertON) {
            lockSwitchState.setText("门未上锁");
        }
        switchImg.setPixelMap(ResourceTable.Media_icon_switch_stop);
    }


    private void masterPasswordDialog() {
        CommonDialog commonDialog = new CommonDialog(getContext());
        commonDialog.setAlignment(LayoutAlignment.BOTTOM);
        commonDialog.setSize(Util.vp2px(getContext(), 336), Util.vp2px(getContext(), 250));
        commonDialog.setCornerRadius(Util.vp2px(getContext(), 24));
        Component container = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_change_master_password,
                null, false);
        commonDialog.setContentCustomComponent(container);
        commonDialog.setAutoClosable(true);
        commonDialog.show();
        addMaterContactsListener(container, commonDialog);
    }

    private void addMaterContactsListener(Component component, CommonDialog Dialog) {
        Button cancelImg = (Button) component.findComponentById(ResourceTable.Id_master_password_button_cancel);
        Button okImg = (Button) component.findComponentById(ResourceTable.Id_master_password_button_save);
        TextField master = (TextField) component.findComponentById(ResourceTable.Id_add_master_password);

        cancelImg.setClickedListener(component1 -> Dialog.destroy());
        okImg.setClickedListener(component12 -> {
            masterPwd.setText(master.getText());
            sendCommand(CommandUtil.getPasswordChangeCommand(userPhone, "U", 0, 0, 0, masterPwd.getText()));
            PreferencesUtil.putString(getContext(), KEY_CHANGE_MASTER_CODE, masterPwd.getText());
            Dialog.destroy();
        });
    }

    private void changeMasterPassword() {
        if (isTemporaryUser == 0) {
            masterPasswordDialog();
        } else {
            XToastUtils.warning("临时用户不能编辑主密码");
        }
    }

    private void tempPasswordDialog() {
        CommonDialog commonDialog = new CommonDialog(getContext());
        commonDialog.setAlignment(LayoutAlignment.BOTTOM);
        commonDialog.setSize(Util.vp2px(getContext(), 336), Util.vp2px(getContext(), 500));
        commonDialog.setCornerRadius(Util.vp2px(getContext(), 24));
        Component container = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_change_temp_password,
                null, false);
        commonDialog.setContentCustomComponent(container);
        commonDialog.setAutoClosable(true);
        commonDialog.show();
        addTempContactsListener(container, commonDialog);
    }

    private void addTempContactsListener(Component component, CommonDialog Dialog) {
        Button cancelImg = (Button) component.findComponentById(ResourceTable.Id_temp_password_button_cancel);
        Button okImg = (Button) component.findComponentById(ResourceTable.Id_temp_password_button_save);
        TextField temp = (TextField) component.findComponentById(ResourceTable.Id_add_temp_password);

        cancelImg.setClickedListener(component1 -> Dialog.destroy());
        okImg.setClickedListener(component12 -> {
            tempPwd.setText(temp.getText());
            addTempPasswordSubmit.setAlpha(1);
            Dialog.destroy();
        });
    }

    private void changeTempPassword() {
        tempPasswordDialog();
    }

    private void submitTempPassword() {
        sendCommand(CommandUtil.getPasswordChangeCommand(userPhone, "U", 1, startTimeToMinute, endTimeToMinute, tempPwd.getText()));
        PreferencesUtil.putString(getContext(), KEY_CHANGE_TEMP_CODE, tempPwd.getText());
        addTempPasswordSubmit.setAlpha((float) 0.4);
    }

    private void openEditTempSwitch() {
        if (isTemporaryUser == 0) {
            isTempON = !isTempON;
            if (isTempON) {
                addTempPasswordSwitch.setPixelMap(ResourceTable.Media_img_switch_on);
                tempPasswordShow.setVisibility(Component.VISIBLE);
            } else {
                addTempPasswordSwitch.setPixelMap(ResourceTable.Media_img_switch_off);
                tempPasswordShow.setVisibility(Component.HIDE);
            }
        } else {
            XToastUtils.warning("临时用户不能编辑临时密码");
        }
    }

    private void goBack(Component component) {
        onBackPressed();
    }
    private void goInfo(Component component) {

       XToastUtils.info("实现输入密码开锁，支持修改主密码，设置并修改临时密码，接受密码被重置和门未关锁的告警消息",5000);
    }

    /**
     * 创建时间选择器
     *
     * @param text 需要修改文本的组件
     */
    private void createTimePicker(Text text, String flag) {
        CommonDialog commonDialog = new CommonDialog(getContext());
        Component dialog = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_dialog_time_pick, null, true);
        commonDialog.setSize(AttrHelper.vp2px(320, getContext()), AttrHelper.vp2px(264, getContext()));
        commonDialog.setContentCustomComponent(dialog);

        TimePicker timePicker = (TimePicker) dialog.findComponentById(ResourceTable.Id_dialog_tp_picker);
        timePicker.showSecond(false);
        dialog.findComponentById(ResourceTable.Id_dialog_btn_confirm).setClickedListener(component -> {
            updateTimeText(text, timePicker.getHour(), timePicker.getMinute(), flag);
            commonDialog.destroy();
        });
        dialog.findComponentById(ResourceTable.Id_dialog_btn_cancel).setClickedListener(component -> commonDialog.destroy());
        commonDialog.show();
    }

    private void updateTimeText(Text text, int hour, int minute, String flag) {
        addTempPasswordSubmit.setAlpha(1);
        text.setText(formatTime(hour) + ":" + formatTime(minute));
        if (flag.equals("start")) {
            startTimeText.setText(formatTime(hour) + ":" + formatTime(minute));
            startTimeToMinute = hour * 60 + minute;
        } else if (flag.equals("end")) {
            endTimeText.setText(formatTime(hour) + ":" + formatTime(minute));
            endTimeToMinute = hour * 60 + minute;
        }
    }

    private String formatTime(int time) {
        if (time < 10) {
            return "0" + time;
        } else {
            return "" + time;
        }
    }

    private void sendCommand(Command command) {
        JSONObject body = new JSONObject();
        body.put("commandName", command.getCommandName());
        body.put("serviceId", command.getServiceId());
        body.put("value", command.getValue());
        OKHttpUtilsRx2.INSTANCE.getApi().sendCommand(deviceId,
                RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), JSON.toJSONString(body)))
                .compose(OKHttpUtilsRx2.INSTANCE.ioMain())
                .subscribe(new OKHttpUtilsRx2.SubjectImpl<RespondBase>() {
                    @Override
                    public void onNext(RespondBase respond) {
                        super.onNext(respond);
                        if (respond.isSuccess()) {
                            XToastUtils.toast("控制命令发送成功");
                        }
                    }
                });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        getOpenLockResult.cancel();
        getOpenLockResult = null;
        getOpenLockResultTask = null;
    }
}

