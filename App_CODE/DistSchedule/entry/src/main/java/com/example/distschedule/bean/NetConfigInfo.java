/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.distschedule.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xuhao.didi.core.iocore.interfaces.ISendable;

import java.nio.charset.Charset;

public class NetConfigInfo implements ISendable {
    private String ssid = "";
    private String password = "";

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public NetConfigInfo(String ssid, String password){
        setPassword(password);
        setSsid(ssid);
    }

    @Override
    public byte[] parse() {
        JSONObject sendData = new JSONObject();
        JSONObject param = new JSONObject();
        param.put("wifiName",getSsid());
        param.put("wifiPassword",getPassword());
        sendData.put("cmd",0x20);
        sendData.put("param",param);
        byte[] body = JSON.toJSONString(sendData).getBytes(Charset.defaultCharset());
        return body;
    }
}
