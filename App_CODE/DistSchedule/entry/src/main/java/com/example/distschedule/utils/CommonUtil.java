/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.distschedule.utils;

import com.example.distschedule.DeviceControlAbility;
import com.example.distschedule.database.DeviceData;
import com.example.distschedule.database.DeviceDataBean;
import com.example.distschedule.toast.XToastUtils;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentState;
import ohos.agp.components.Switch;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.Map;

/**
 * Common util
 *
 * @since 2021-08-21
 */
public class CommonUtil {
    private static final String TAG_LOG = "CommonUtil";

    private CommonUtil() {
    }

    /**
     * show
     *
     * @param context context
     * @param msg     message
     */
    public static void showUiTip(Context context, String msg) {
        context.getUITaskDispatcher().delayDispatch(() -> {
            ToastDialog toastDialog = new ToastDialog(context);
            toastDialog.setAutoClosable(false);
            toastDialog.setContentText(msg);
            toastDialog.show();
        }, 0);
    }

    public static void setSwitchStyle(Switch switchComponent) {
        ShapeElement elementThumbOn = new ShapeElement();
        elementThumbOn.setShape(ShapeElement.OVAL);
        elementThumbOn.setRgbColor(RgbColor.fromArgbInt(0xFFFFFFFF));
        elementThumbOn.setCornerRadius(50);
        // 关闭状态下滑块的样式
        ShapeElement elementThumbOff = new ShapeElement();
        elementThumbOff.setShape(ShapeElement.OVAL);
        elementThumbOff.setRgbColor(RgbColor.fromArgbInt(0xFFFFFFFF));
        elementThumbOff.setCornerRadius(50);
        // 开启状态下轨迹样式
        ShapeElement elementTrackOn = new ShapeElement();
        elementTrackOn.setShape(ShapeElement.RECTANGLE);
        elementTrackOn.setRgbColor(RgbColor.fromArgbInt(0xFF0A59F7));
        elementTrackOn.setCornerRadius(50);
        // 关闭状态下轨迹样式
        ShapeElement elementTrackOff = new ShapeElement();
        elementTrackOff.setShape(ShapeElement.RECTANGLE);
        elementTrackOff.setRgbColor(RgbColor.fromArgbInt(0xFF808080));
        elementTrackOff.setCornerRadius(50);

        switchComponent.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
        switchComponent.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));
    }

    private static StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }

    private static StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }

    /**
     * 截取小数点后两位
     *
     * @param rateStr 输入数据
     * @return 保留小数点后两位
     */
    public static String getRateStr(String rateStr) {

        int i = rateStr.indexOf('.');
        if (i != -1) {
            //获取小数点后面的数字 是否有两位 不足两位补足两位
            String dianAfter = rateStr.substring(0, i + 1);
            String afterData = rateStr.replace(dianAfter, "");
            if (afterData.length() < 2) {
                afterData = afterData + "0";
            }
            return rateStr.substring(0, i) + "." + afterData.substring(0, 2);
        } else {
            rateStr = rateStr + ".00";
            return rateStr;
        }
    }

    /**
     * 获取图片名称
     *
     * @param component 组件
     * @return 图片名称
     */
    public static String getImageName(Component component) {
        String imageName = "";
        if (component.getTag() instanceof String) {
            imageName = (String) component.getTag();
        }
        return imageName;
    }

    public static void judgeDevice(Context context, String deviceId, Intent intent) {
        if (intent == null) {
            intent = new Intent();
        }
        intent.setParam("deviceId", deviceId);
        try {
            Map<String, DeviceDataBean> map = DeviceData.INSTANCE.getDeviceData();
            String productId = deviceId.split("_")[0];
            toControlDevice(context, map.get(productId).getAction(), intent);
        } catch (Exception e) {
            XToastUtils.warning("跳转失败错误");
        }
    }

    private static void toControlDevice(Context context, String action, Intent intent) {
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withAbilityName(DeviceControlAbility.class.getName())
                .withAction(action)
                .build();
        intent.setOperation(operation);
        context.startAbility(intent, 0);
    }

    /**
     * Num 数字常量类
     *
     * @since 2021-05-10
     */
    public static class Num {
        /**
         * read bytes num
         */
        public static final int READ_BYTES_NUM = -1;

        /**
         * bytes num
         */
        public static final int BYTES_NUM = 1024;

        /**
         * two
         */
        public static final int TWO = 2;

        /**
         * three
         */
        public static final int THREE = 3;

        /**
         * four
         */
        public static final int FOUR = 4;

        /**
         * six
         */
        public static final int SIX = 6;

        /**
         * eight
         */
        public static final int EIGHT = 8;

        /**
         * sixteen
         */
        public static final int SIXTEEN = 16;
    }
}
