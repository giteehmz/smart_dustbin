package com.example.distschedule.utils;

import com.huaweicloud.sdk.core.auth.ICredential;
        import com.huaweicloud.sdk.core.auth.BasicCredentials;
        import com.huaweicloud.sdk.core.exception.ConnectionException;
        import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
        import com.huaweicloud.sdk.core.exception.ServiceResponseException;
        import com.huaweicloud.sdk.iotda.v5.region.IoTDARegion;
        import com.huaweicloud.sdk.iotda.v5.*;
        import com.huaweicloud.sdk.iotda.v5.model.*;


public class ShowDeviceShadowSolution {

    public static void main(String[] args) {
        String ak = "<YOUR AK>";
        String sk = "<YOUR SK>";

        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        IoTDAClient client = IoTDAClient.newBuilder()
                .withCredential(auth)
                .withRegion(IoTDARegion.valueOf("cn-north-4"))
                .build();
        ShowDeviceShadowRequest request = new ShowDeviceShadowRequest();
        request.withDeviceId("61d2add4a61a2a029ccbe02d_SmartDustbin001");
        try {
            ShowDeviceShadowResponse response = client.showDeviceShadow(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}