/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.distschedule;

import com.example.distschedule.slice.AddDeviceAbilitySlice;
import com.example.distschedule.slice.AlarmAbilitySlice;
import com.example.distschedule.slice.OtherWifiSlice;
import com.example.distschedule.slice.SocketNetConfigSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class AddDeviceAbility extends Ability {
    public static final String ACTION_ALARM = "action.alarm";
    public static final String ACTION_SOCKET = "action.socketNetConfig";
    public static final String ACTION_OTHER_WIFI = "action.otherWifi";
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(AddDeviceAbilitySlice.class.getName());
        addActionRoute(ACTION_ALARM, AlarmAbilitySlice.class.getName());
        addActionRoute(ACTION_SOCKET, SocketNetConfigSlice.class.getName());
        addActionRoute(ACTION_OTHER_WIFI, OtherWifiSlice.class.getName());
    }
}
