# **学生党上手数字管家 — — 碰一碰让台灯亮起来！**  
>——以下是学生党个人总结的方法，参考了Gitee中数字管家的Demo文档并进行二次分析和总结，同时分享出来，希望能和大家一起进步。星星之火，点亮鸿蒙！

在开始上手之前，我需要通过架构图带着大家来简单了解一下**数字管家**，下面为大家放上一张数字管家的结构图，帮助同学们在学习的时候有条不紊的梳理知识点和发现问题。

![1.png](./resources/1.png)

接下来为大家揭晓上手的关键，关键就在于**软件安装、硬件烧录、NFC配置**。其中**密钥、签名、证书文件的申请及配置**和**NFC配置**是**2个小难点**，所以在这篇学习心得分享上会着重提到我们遇到的这2个难点以及应对的办法。
### 大体步骤如下：

![2.png](./resources/2.png)
#### 难点 · 1 · 密钥文件、签名文件、证书文件的申请及配置流程
![3.png](./resources/3.png)
注：图中每条路径上均为 〈目的 | 解决路径  方向〉 的排版方式，解决顺序由上往下（黄、蓝、粉）依次进行。

#### 难点 · 2 · NFC配置流程
![4.png](./resources/4.png)
注：图中路径标签以及解决顺序同上。

**需要具备的条件：**

1、搭载HarmonyOS的华为手机，并且API版本在6以上，而且还要有NFC功能。（API的查询文档中会有详细说明）

2、小熊派Hi3861开发板以及E53_SC1智慧台灯模块。



**[ 温馨提示：建议同学们先看图，看懂了接下来就好办了。]**



### **具体步骤如下：**

#### 第一步：代码下载。

下载源：

https://gitee.com/openharmony-sig/knowledge_demo_smart_home

![5.png](./resources/5.png)
#### 第二步：IDE安装。

下载源：

https://developer.harmonyos.com/cn/develop/deveco-studio

![6.png](./resources/6.png)
**·安装后配置SDK版本为6以上即可**
![7.png](./resources/7.png)



#### 第三步：密钥、签名、证书文件的申请及配置
##### ·首先在AppGallery Connect中创建项目

https://developer.huawei.com/consumer/cn/service/josp/agc/index.html
![8.png](./resources/8.png)
![9.png](./resources/9.png)
![10.png](./resources/10.png)
![11.png](./resources/11.png)
![12.png](./resources/12.png)
![13.png](./resources/13.png)

**·点击确认后，添加.JSON文件，请一定记住是放在entry之下。**

![14.png](./resources/14.png)
![15.png](./resources/15.png)

注：无论是重构添加还是复制添加，请一定放在entry之下。

##### ·分别在三个文件中把bundleName中的包名修改为自己定义的包名。

**1.DistSchedule/entry/src/config.json**

![16.png](./resources/16.png)

**2.DistSchedule/netconfig/src/main/config.json**

![17.png](./resources/17.png)

**3.DistSchedule/netconfig/src/main/js/default/pages/index/index.js**

![18.png](./resources/18.png)

现在我们开始获取三个重要文件：p12、cer、p7b

##### **·获取密钥. csr和签名.p12**

·在Build（构建选项栏）中选择Generate Key and CSR选项，New创建新的密钥和签名文件。

![19.png](./resources/19.png)

**将.p12文件放在心仪的文件位置。Password为密码，自定义即可。**

![20.png](./resources/20.png)

**·First and last name 这一项必须填写，自定义即可。**
![21.png](./resources/21.png)

**·同样将.csr放在自己心仪的文件夹下。**

![22.png](./resources/22.png)

**·现在我们的密钥. csr和签名.p12就弄好了。**

**~注：各种文件存放的位置一定要记住呦！**




##### ·获取手机的DUID

**·来到手机端，点击版本号6下进入开发者模式，打开USB调试，并将手机和PC端连接。**

![23.png](./resources/23.png)




**·来到SDK窗口，复制文件路径。**

![24.png](./resources/24.png)

**·来到文件窗口进入toolchains文件夹，全选文件路径并输入cmd，进入命令窗口。**

![25.png](./resources/25.png)

**·输入hdc shell bm get -u，就得到了手机的UDID。**

![26.png](./resources/26.png)

~注：UDID获取后记得保存在文本文档里呦！方便后面使用。

如需查看手机的API版本号，则输入hdc shell 再输入getprop hw_sc.build.os.apiversion就可以获取了，手机的API版本号需要6以上的才能打开数字管家APP进行测试呦！


##### ·获取证书.cer文件（这里会用到手机的UDID和.csr文件）
·来到AGC设备管理平台，添加设备。
![27.png](./resources/27.png)

![28.png](./resources/28.png)

**·之后转到证书管理平台，上传证书。**

![29.png](./resources/29.png)

·下载出来的文件就是.cer。到此，我们的证书.cer也弄好了。

~接下来就剩.p7b文件了。

##### ·获取证书.p7b文件（这里会用到已配置的手机和.cer文件）
**·来到HAP平台。**

![30.png](./resources/30.png)

![31.png](./resources/31.png)

![32.png](./resources/32.png)

![33.png](./resources/33.png)
·添加的操作很简便的，所以就不详细讲了。下载之后就可以得到.p7b文件了。

**·到这一步，我们获取三个重要文件.p12、cer、p7b就完成了，接下来就是来到IDE中进行配置。**


##### 在IDE中进行配置
来到DevEco Studio（先登录），在 File （文件选项栏）> Project Structure > Project >Signing Configs

**·自动配置的可以在文件夹.ohos\config中查看。（真机连接的时候才可以自动配置）**

![34.png](./resources/34.png)

#### 第四步：跑起来！

来到运行/调试配置窗口，选择entry并勾选上Deploy Multi Hap Packages

![35.png](./resources/35.png)

![36.png](./resources/36.png)



成功之后你将会在手机上看见数字管家的APP。



![37.png](./resources/37.png)

**如果没有成功，请细细品味难点1中的流程图。你品，你细品！**

**到这一步的小伙伴一定很高兴对吧！我也为你感到高兴。但目前才完成了三分之一，不过离成功就只有简单的三分之二了，加油！**



#### 第五步：硬件烧录
**·USB串口驱动，下载源：http://www.wch.cn/search?q=ch340g&t=downloads**
**安装好之后连接开发板，就可以在设备管理器中查看串口号，例如我的是COM3。**

![38.png](./resources/38.png)

**·之后安装HiBurn烧录软件，下载源：https://harmonyos.51cto.com/resource/29**
**安装完毕后，打开HiBurn**

![39.png](./resources/39.png)

Setting->Com settings->Baud —— 波特率设置

COM —— 串口选择

Refresh —— 刷新串口

Select file —— 烧录文件选择

Connect —— 烧录连接

图中COM为串口选择，如果已连接开发板，但软件没有发现就点一下Refresh；
Select file为烧录文件位置选择，这里我们选择代码路径下:
**knowledge_demo_smart_home\dev\docs\quick_start\resource\image\Hi3861_wifiiot_app_allinone.bin**
点击Connect之后按一下开发板上的复位件RESET等待进度条跑完，就烧录好了。




#### 第六步：最后一步NFC配置（其中FA信息配置和NFC写入内容尤为重要）

##### 来到华为开发者联盟中的华为快服务智慧平台，创建服务模型。

![40.png](./resources/40.png)

![41.png](./resources/41.png)

![42.png](./resources/42.png)

![43.png](./resources/43.png)
##### 创建完模型之后，进行信息配置
**一、服务信息配置**

![44.png](./resources/44.png)

![45.png](./resources/45.png)

**二、标签配置**


![46.png](./resources/46.png)

![47.png](./resources/47.png)

**三、测试配置**

![48.png](./resources/48.png)

![49.png](./resources/49.png)

确定之后开启调试即可。

##### 最后一步：手机端碰一碰写入NFC信息

·需要下载应用调测助手APP，申请Product ID，将已有的智慧台灯的标签内容先写入自定义缓存区，再去碰NFC芯片。

·已有的智慧台灯的自定义数据：1246128c7b60ad1ed0286680f19206Lamp01308123456784011512teamX-Lamp01

![50.png](./resources/50.png)

**NFC标签的自定义数据是否按照如下格式。（引用于https://gitee.com/openharmony-sig/knowledge_demo_smart_home/dev/docs/NFC_label_definition/README.md）**

| 标签代号 | 意义                | 限制条件                                                     | 样例                                                         |
| -------- | ------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1        | IoTDA平台设备品类ID | 典型值24字节，小于32字节                                     | 6128c7b60ad1ed0286680f19                                     |
| 2        | NodeID              | 典型值8字节，小于64字节                                      | Lamp01                                                       |
| 3        | DevicePwd           | 典型值8字节，小于32字节                                      | 12345678                                                     |
| 4        | 配网标识            | 1字节                                                        | 0：不需要配网设备（如手表平板等自带蜂窝网络的设备）；<br />1：NAN配网（能自动使用当前网络配网，不需要输入密码）;<br />2：softAp配网;<br />3：ble蓝牙配网; |
| 5        | ApSSID              | 设备自己发出的热点名，典型值12字节，小于32字节，NAN配网和softAp配网必须提供；构成一般为前缀teamX + 设备ID模式 | teamX-Lamp01                                                 |
| 6        | ApToken             | 典型值8字节，小于16字节；WIFI配网下必须提供（可以置空）      | 12345678                                                     |
| 7        | BLE-MAC             | 典型值6字节；BLE配网下必须提供                               | 010203040506                                                 |

###### 小知识点：NFC标签内容写入技巧（可以拿小本本记下来，有助于接下来的学习）

![NFC写入.png](./resources/NFC写入.png)



**··当屏幕前的你完成到这一步时。**
**恭喜你！快去！接通开发板的电源，赶紧碰一碰点亮你的小台灯吧 ^^**

![52.png](./resources/52.png)

图中是两个手机连接一个设备，并且都拥有控制权。感兴趣的小伙伴可以去试试！
至于怎么弄出来的，嘻嘻，我不说，自己去探索。

#### 个人寄语：

>文档当中仍然有很多不足之处，所以我将之定义为“学生党上手数字管家1.0”，分享出来的目的是希望大家能一起将文档更新为2.0、3.0……一直到最后的END版本。我们可以将各自总结的知识点融汇在一起，一起打造一个巨大的笔记本，让这本笔记记录我们的成长，也让我们的努力变得有迹可循！

​           