# Smart_dustbin

### 介绍
智能语音垃圾箱
​基于OpenHarmony 开发的语音识别分类垃圾桶，其中主要包括语音识别模块、容量检测模块，IoT云控制模块，无线配网模块这四个模块。


![framwork](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/framwork.png)


智能语音分类垃圾桶通过MQTT协议连接华为IOT物联网平台，从而实现命令的接收和属性上报。智能设备同数字管家应用之间的设备模型定义可以参考[profile](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/%E6%99%BA%E8%83%BD%E8%AF%AD%E9%9F%B3%E5%9E%83%E5%9C%BE%E6%A1%B6%20Profile%E5%AE%9A%E4%B9%89.md)

### 目录结构
```
.
├── Dev_CODE
│   ├── smart_dusbtin // 设备代码
├── App_CODE
│   └── DistSchedule  // 数字管家代码
│   └── Smart_Dustbin // 智能语音垃圾桶页面
│   
├── DOC // Smart_Dutbin的开发文档
└── 
```

### 安装教程
#### 编译Smart_Dustbin设备代码
######     1. 搭建好环境后，下载Openharmony1.01源码后
######     2. 将Dev_CODE里面的Smart_Dustin复制到team_X文件夹里面
######     3. 在源码的根目录下打开命令窗口，输入"hb set"，选择Smart_Dustbin，输入"hb build"进行编译
######     4. 编译成功后在源码目录/out/hispark_pegasus/smart_dustbin下找到Hi3861_wifiiot_app_allinone.bin文件
######     5. bin文件可以通过Hiburn进行烧录
#### 在数字管家添加Smart_Dusbtin控制页面
######     1. 在DistSchedule\entry\src\main\java\com\example\distschedule\slice中添加Smart_Dustbin的DustbinAbilitySlice.java控制页
######     2. 在\DistSchedule\entry\src\main\resources\base\layout里面添加Smart_Dustbin的ability_dustbin.xml布局
######     3. 在DeviceControlAbility添加路由
```java
public static final String ACTION_DUSTBIN = "action.dustbin";        //添加至全局变量
addActionRoute(ACTION_DUSTBIN, DustbinAbilitySlice.class.getName()); //智能语音垃圾桶
```
### 使用说明
  Smart_Dustbin主要功能是可以语音控制垃圾桶开盖，和用户远程控制；使用一级命令“你好，垃圾桶”，随后后说出二级命令“可回收垃圾”、“不可回收垃圾”、“厨余回收垃圾”、“其他回收垃圾”识别后，打开对应的垃圾桶盖等待10秒后，桶盖自动关闭；还可以通过数字管家查看垃圾桶容量，对语音分类垃圾桶进行控制，让垃圾回收处理变得不再繁琐，回收人员只需要将对应的垃圾箱里的垃圾拿出即可。有效的减少了垃圾分类的工作量，给社会带来了很大的便捷。


### 文档目录
#### 开发文档：

[【智能语音垃圾桶开发】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/%E6%99%BA%E8%83%BD%E8%AF%AD%E9%9F%B3%E5%9E%83%E5%9C%BE%E6%A1%B6%E5%BC%80%E5%8F%91.md)

[【智能语音垃圾桶——创建IoT云】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/%E6%99%BA%E8%83%BD%E8%AF%AD%E9%9F%B3%E5%9E%83%E5%9C%BE%E6%A1%B6%E2%80%94%E2%80%94%E5%88%9B%E5%BB%BAIoT%E4%BA%91.md)

[【智能语音垃圾桶——设备端】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/%E6%99%BA%E8%83%BD%E8%AF%AD%E9%9F%B3%E5%9E%83%E5%9C%BE%E6%A1%B6%E2%80%94%E2%80%94%E8%AE%BE%E5%A4%87%E7%AB%AF.md)

[【智能语音垃圾桶——数字管家】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/%E6%99%BA%E8%83%BD%E8%AF%AD%E9%9F%B3%E5%9E%83%E5%9C%BE%E6%A1%B6%E2%80%94%E2%80%94%E6%95%B0%E5%AD%97%E7%AE%A1%E5%AE%B6.md)

[【数字管家应用的编译与安装】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/%E6%95%B0%E5%AD%97%E7%AE%A1%E5%AE%B6%E5%BA%94%E7%94%A8%E7%9A%84%E7%BC%96%E8%AF%91%E4%B8%8E%E5%AE%89%E8%A3%85.md)

[【Gitee使用方法】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/Gitee%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95.md)

#### 设备调试及问题：

[【hi3861的舵机控制】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/Questions/hi3861%E7%9A%84%E8%88%B5%E6%9C%BA%E6%8E%A7%E5%88%B6.md)

[【润和hi3861开发环境的搭建】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/Questions/%E6%B6%A6%E5%92%8Chi3861%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E7%9A%84%E6%90%AD%E5%BB%BA.md)

[【hi3861的传感器控制】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/Questions/hi3861%E7%9A%84%E4%BC%A0%E6%84%9F%E5%99%A8%E6%8E%A7%E5%88%B6.md)

[【点亮第一个LED灯】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/Questions/%E7%82%B9%E4%BA%AE%E7%AC%AC%E4%B8%80%E4%B8%AALED%E7%81%AF.md)

[【Q-提示未知数据库】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/Questions/Q-%E6%8F%90%E7%A4%BA%E6%9C%AA%E7%9F%A5%E6%95%B0%E6%8D%AE%E5%BA%93.md)


[【模拟机调试数字管家页面】](https://gitee.com/giteehmz/smart_dustbin/blob/master/DOC/%E6%A8%A1%E6%8B%9F%E6%9C%BA%E8%B0%83%E8%AF%95%E6%95%B0%E5%AD%97%E7%AE%A1%E5%AE%B6%E9%A1%B5%E9%9D%A2.md)


### 演示：
![netConfig](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/video/netConfig.GIF)

[【视频链接】](https://www.bilibili.com/video/av211684398/)

[【视频链接】](https://www.bilibili.com/video/av936708368/)


有任何问题可联系：2283082325@qq.com