/*
 * Copyright (c) 2022 zhangbing LaLa_Team.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "control.h"
#include <hi_adc.h>
#include <hi_time.h>
#include <hi_wifi_api.h>
#include "ohos_types.h"
#include "log.h"
#include "oled.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "cmsis_os2.h"
#include "iot_cloud.h"
#include "iot_profile.h"
#include "cJSON.h"
#include <hi_uart.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "peripheral_hal.h"
#include "control.h"
#include "iot_uart.h"

#define SMART_BIN_SLEEP_2500MS  2500
#define UART_TASK_STAK_SIZE 1024*6

#define UART_NUM            HI_UART_IDX_1
#define UART_BUFF_SIZE           256

#define CONFIG_TASK_UART_PRIOR           20                        /* default task priority */
#define UART_BAUD_RATE                   9600                      //  115200
#define UART_DATA_BITS                   8
#define UART_STOP_BITS                   1
#define WIFI_IOT_IO_FUNC_GPIO_0_UART1_TXD   2
#define WIFI_IOT_IO_FUNC_GPIO_1_UART1_RXD   2

uint8_t g_uart_buff[UART_BUFF_SIZE] = {0};
uint32_t g_ReceivedDatalen = 0;


typedef struct
{
    bool isReadBusy;
} UartController;



static UartController uartController;


static void setVoiceCommand(void)
{  
    if (strstr(g_uart_buff, "Open_R") != NULL)
    {
        printf("[Dustbin_tes1]listened Open_R\n");
        Dustbin_open_R();
        printf("[Dustbin_tes1] Delay 10s close_R\n");
        osDelay(200);
        Dustbin_close_R();
        
    }else if (strstr(g_uart_buff, "Open_U") != NULL)
    {
        printf("[Dustbin_tes1]listened Open_U\n");
        Dustbin_open_U();
        printf("[Dustbin_tes1] Delay 10s close_U\n");
        osDelay(200);
        Dustbin_close_U();
    }else if (strstr(g_uart_buff, "Open_K") != NULL)
    {
        printf("[Dustbin_tes1]listened Open_K\n");
        Dustbin_open_K();
        printf("[Dustbin_tes1] Delay 10s close_K\n");
        osDelay(200);
        Dustbin_close_K();
    }else if (strstr(g_uart_buff, "Open_O") != NULL)
    {
        printf("[Dustbin_tes1]listened Open_O\n");
        Dustbin_open_O();
        printf("[Dustbin_tes1] Delay 10s close_O\n");
        osDelay(200);
        Dustbin_close_O();
    }

}


static void UartReceiveMessage(void)
{
    char *recData;
    printf("----Listening----\n");
    RaiseLog(LOG_LEVEL_INFO,"Start Listening serial port");

    if (UartIsBufEmpty())
        {
            return;
        }
        if (uartController.isReadBusy)
        {
            return;
        }

        uartController.isReadBusy = true;
        g_ReceivedDatalen = hi_uart_read(UART_NUM, g_uart_buff, UART_BUFF_SIZE);

        if (g_ReceivedDatalen > 0)
        {
            printf("[Dustbin_tes1]handleUartReceiveMessage rcvData len:%d,msg:%s.\n", g_ReceivedDatalen, g_uart_buff);
            
            setVoiceCommand();//Setting voice Commands

            memset(g_uart_buff, 0, sizeof(g_uart_buff));
            g_ReceivedDatalen = 0;
        }

        uartController.isReadBusy = false;
}


int UartIsBufEmpty(void)
{
    RaiseLog(LOG_LEVEL_INFO,"isBufEmpty");
    bool val = false;
    int ret;

    ret = hi_uart_is_buf_empty(HI_UART_IDX_1, &val);
    if (ret == HI_ERR_SUCCESS && val == HI_TRUE)
    {
        return 1;
    }

    return 0;
}

static void *uart_demo_task(void)
{
    static uint16_t countSendTimes = 0;
    static uint8_t countReceiveTimes = 0;
    uartController.isReadBusy = false;
    printf("[Initialize uart successfully\n");
    while (1)
    {
        osDelay(50); 
        UartReceiveMessage();//Collecting Serial Port Data
        hi_sleep(SMART_BIN_SLEEP_2500MS);  
    }
    return 0;
}

void uart_task_create(void)
{
    RaiseLog(LOG_LEVEL_INFO,"uart_task_create");
    osThreadAttr_t attr;

    attr.name = "uartTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = UART_TASK_STAK_SIZE;
    attr.priority = CONFIG_TASK_UART_PRIOR;

    if (osThreadNew((osThreadFunc_t)uart_demo_task, NULL, &attr) == NULL)
    {
        printf("[Dustbin_tes1]Falied to create uart_demo_task!\n");
    }
}


 void UartInit(void){
    RaiseLog(LOG_LEVEL_INFO,"[2022012x01] entry into UartInit");
    IoTGpioInit(HAL_WIFI_IOT_IO_NAME_GPIO_0);
    HalIoSetFunc(HAL_WIFI_IOT_IO_NAME_GPIO_0, WIFI_IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoTGpioInit(HAL_WIFI_IOT_IO_NAME_GPIO_1);
    HalIoSetFunc(HAL_WIFI_IOT_IO_NAME_GPIO_1, WIFI_IOT_IO_FUNC_GPIO_1_UART1_RXD);
    
    hi_uart_attribute uart_attr = {
        .baud_rate = UART_BAUD_RATE, /* baud_rate: 9600 */
        .data_bits = UART_DATA_BITS,      /* data_bits: 8bits */
        .stop_bits = UART_STOP_BITS,
        .parity = 0,
    };

    RaiseLog(LOG_LEVEL_INFO,"[2022012x01] uart_init success");
    /* Initialize uart driver */
    hi_u32 ret = hi_uart_init(HI_UART_IDX_1, &uart_attr, HI_NULL);
    if (ret != HI_ERR_SUCCESS)
    {
        printf("[Dustbin_tes1]Failed to init uart! Err code = %d\n", ret);
        return;
    }
}
//APP_FEATURE_INIT(uart_task_create);