/*
 * Copyright (c) 2022 zhangbing LaLa_Team.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <hi_adc.h>
#include <hi_time.h>
#include <hi_wifi_api.h>
#include "control.h"
#include "ohos_types.h"
#include "log.h"
#include "oled.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "cmsis_os2.h"
#include "iot_cloud.h"
#include "iot_profile.h"
#include "cJSON.h"
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "peripheral_hal.h"
#include "utils_hal.h"
#include "cmsis_os2.h"
#include "iot_adc.h"

#define TRASH_DEMO_TASK_STACK_SIZE (1024*4)
#define TRASH_DEMO_TASK_PRIOPITY   (26)
#define ADC_TEST_LENGTH            (20)
#define VLT_MIN (100)
#define SMART_CAN_SLEEP_2500MS  2500
#define Y_SHOW_SMART_CAN_STATUS 6
#define Y_SHOW_SMART_CAN_SHOW_OPEN 4
#define THE_PARM_OF_IR_SENSOR 3.1
#define THE_MAX_PARM_OF_DISTANCE_SENSOR 3.4
#define THE_MIN_PARM_OF_DISTANCE_SENSOR 3.3
#define THE_PARM_OF_CACL_CYCLE 20
#define THE_CYCLES_OF_PWM_CONTROL 20000
#define THE_ANGLE_OF_dustbin_WROK 1000

uint32 g_num = 0;
bool   g_dustbinIsOpen = FALSE;
float  g_Capacityvalue = 0;
hi_u16  g_adc_buf_trash[ADC_TEST_LENGTH] = { 0 };

void GpioInitOperation(HalWifiIotIoName name, uint8 val, IotGpioDir dir, IotGpioValue value)
{
    IoTGpioInit(name);
    HalIoSetFunc(name, val);
    IoTGpioSetDir(name, dir);
    if (IOT_GPIO_DIR_OUT == dir) {
        IoTGpioSetOutputVal(name, value);
    }
}

float get_adc_value_N(HalWifiIotAdcChannelIndex channel)
{
    uint32 ret = 0;
    hi_u16 data = 0;
    hi_u16 vlt = 0;
    float voltage = 0;
    float vlt_max = 0;
    float vlt_min = VLT_MIN;

    for(uint32 i = 0; i < ADC_TEST_LENGTH; i++) {
        ret = HalAdcRead(channel, &data, HAL_WIFI_IOT_ADC_EQU_MODEL_4, HAL_WIFI_IOT_ADC_CUR_BAIS_DEFAULT, 0);
        if (ret = IOT_SUCCESS) {
            //printf("[Dustbin_tes1]HalAdcRead failed%d\n",(unsigned int)data);
        }
        //printf("[Dustbin_tes1]getADC 0_Value_N%d\n",(unsigned int)data);
        g_adc_buf_trash[i] = data;
    }

    for (uint32 i = 0; i < ADC_TEST_LENGTH; i++){
        vlt = g_adc_buf_trash[i];
        voltage = (float)vlt * 1.8 * 4 / 4096.0;
        vlt_max = (voltage > vlt_max) ? voltage : vlt_max;
        vlt_min = (voltage < vlt_min) ? voltage : vlt_min;
    }
    printf("[Dustbin_tes1]ADC 0_value_N vlt_max%f\n",vlt_max);
    return vlt_max;
}
float get_adc_value_S(HalWifiIotAdcChannelIndex channel)
{
    uint32 ret = 0;
    hi_u16 data = 0;
    hi_u16 vlt = 0;
    float voltage = 0;
    float vlt_max = 0;
    float vlt_min = VLT_MIN;

    for(uint32 i = 0; i < ADC_TEST_LENGTH; i++) {
        ret = HalAdcRead(channel, &data, HAL_WIFI_IOT_ADC_EQU_MODEL_4, HAL_WIFI_IOT_ADC_CUR_BAIS_DEFAULT, 0);
        if (ret = IOT_SUCCESS) {
            printf("[Dustbin_tes1]HalAdcRead failed%d\n",(unsigned int)data);
        }
        //printf("[Dustbin_tes1]getADC 6_Value_S%d\n",(unsigned int)data);
        g_adc_buf_trash[i] = data;
    }

    for (uint32 i = 0; i < ADC_TEST_LENGTH; i++){
        vlt = g_adc_buf_trash[i];
        voltage = (float)vlt * 1.8 * 4 / 4096.0;
        vlt_max = (voltage > vlt_max) ? voltage : vlt_max;
        vlt_min = (voltage < vlt_min) ? voltage : vlt_min;
    }
    printf("[Dustbin_tes1]ADC 6_value_S vlt_max%f\n",vlt_max);
    return vlt_max;
}
float get_adc_value_C(HalWifiIotAdcChannelIndex channel)
{
    uint32 ret = 0;
    hi_u16 data = 0;
    hi_u16 vlt = 0;
    float voltage = 0;
    float vlt_max = 0;
    float vlt_min = VLT_MIN;

    for(uint32 i = 0; i < ADC_TEST_LENGTH; i++) {
        ret = HalAdcRead(channel, &data, HAL_WIFI_IOT_ADC_EQU_MODEL_4, HAL_WIFI_IOT_ADC_CUR_BAIS_DEFAULT, 0xF0);
        if (ret = IOT_SUCCESS) {
            printf("[Dustbin_tes1]HalAdcRead failed%d\n",(unsigned int)data);
        }
        //printf("[Dustbin_tes1]getADC 5_Value_C%d\n",(unsigned int)data);
        g_adc_buf_trash[i] = data;
    }

    for (uint32 i = 0; i < ADC_TEST_LENGTH; i++){
        vlt = g_adc_buf_trash[i];
        voltage = (float)vlt * 1.8 * 4 / 4096.0;
        vlt_max = (voltage > vlt_max) ? voltage : vlt_max;
        vlt_min = (voltage < vlt_min) ? voltage : vlt_min;
    }
    printf("[Dustbin_tes1]ADC 5_value_C vlt_max%f\n",vlt_max);
    return vlt_max;
}

bool is_someone_there(void)
{
    float value = get_adc_value_S(HAL_WIFI_IOT_ADC_CHANNEL_2);
    RaiseLog(LOG_LEVEL_INFO,"someone_value_ADC2",value);
    printf("[Dustbin_tes1]omeone_value_ADC2%f\n",value);
    if (value > THE_PARM_OF_IR_SENSOR) {
        return TRUE;
    }
    return FALSE;
}

bool is_night_time(void)
{
    float value = get_adc_value_N(HAL_WIFI_IOT_ADC_CHANNEL_0);
    RaiseLog(LOG_LEVEL_INFO,"night_time",value);
    printf("[Dustbin_tes1]night_time %f\n",value);
    if (value > THE_PARM_OF_IR_SENSOR) {
        return TRUE;
    }
    return FALSE;
}

bool is_Dustbin_full(void)
{
    g_Capacityvalue = get_adc_value_C(HAL_WIFI_IOT_ADC_CHANNEL_5);
    
    printf("[Dustbin_tes1]Dustbin_C value:   %f\n",g_Capacityvalue);
    if(g_Capacityvalue < THE_MAX_PARM_OF_DISTANCE_SENSOR && g_Capacityvalue >= THE_MIN_PARM_OF_DISTANCE_SENSOR) {
        printf("[Dustbin_tes1]trash can is full\n");
        return TRUE;
    }
    printf("[Dustbin_tes1]trash can is not full\n");
    return FALSE;
}

/* 设置舵机旋转的角度 */
void set_engine_angle_R(hi_s32 duty)
{   
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Opening_R");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_10, IOT_GPIO_VALUE1);
        hal_udelay(duty - i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_10, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - (duty - i*THE_PARM_OF_CACL_CYCLE));
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Opened_R");
}
void set_engine_angle_U(hi_s32 duty)
{
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Opening_U");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_9, IOT_GPIO_VALUE1);
        hal_udelay(duty - i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_9, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - (duty - i*THE_PARM_OF_CACL_CYCLE));
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Openined_U");
}
void set_engine_angle_K(hi_s32 duty)
{
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Opening_K");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_7, IOT_GPIO_VALUE1);
        hal_udelay(duty - i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_7, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - (duty - i*THE_PARM_OF_CACL_CYCLE));
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Opened_K");
}
void set_engine_angle_O(hi_s32 duty)
{
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Opening_O");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_6, IOT_GPIO_VALUE1);
        hal_udelay(duty - i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_6, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - (duty - i*THE_PARM_OF_CACL_CYCLE));
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] Opened_O");
}

void set_engine_angle_reversal_R(hi_s32 duty)
{
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closing_R");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_10, IOT_GPIO_VALUE1);
        hal_udelay(i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_10, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - i*THE_PARM_OF_CACL_CYCLE);
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closed_R");
}
void set_engine_angle_reversal_U(hi_s32 duty)
{
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closing_U");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_9, IOT_GPIO_VALUE1);
        hal_udelay(i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_9, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - i*THE_PARM_OF_CACL_CYCLE);
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closed_U");
}
void set_engine_angle_reversal_K(hi_s32 duty)
{
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closing_K");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_7, IOT_GPIO_VALUE1);
        hal_udelay(i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_7, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - i*THE_PARM_OF_CACL_CYCLE);
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closed_K");
}
void set_engine_angle_reversal_O(hi_s32 duty)
{
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closing_O");
    uint32 count = duty/THE_PARM_OF_CACL_CYCLE;
    for (int i = 1; i <= count; i++) {
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_6, IOT_GPIO_VALUE1);
        hal_udelay(i*THE_PARM_OF_CACL_CYCLE);
        IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_6, IOT_GPIO_VALUE0);
        hal_udelay(THE_CYCLES_OF_PWM_CONTROL - i*THE_PARM_OF_CACL_CYCLE);
    }
    RaiseLog(LOG_LEVEL_INFO,"[Dustbin_tes1] closed_O");
}

void Dustbin_close_R(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "               ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_R", 1);
    printf("[Dustbin_tes1]R close\n");
    set_engine_angle_reversal_R(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = FALSE;
}
void Dustbin_close_U(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_U", 1);
    printf("[Dustbin_tes1]U close\n");
    set_engine_angle_reversal_U(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = FALSE;
}
void Dustbin_close_K(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_K", 1);
    printf("[Dustbin_tes1]K close\n");
    set_engine_angle_reversal_K(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = FALSE;
}
void Dustbin_close_O(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "           ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:close_O", 1);
    printf("[Dustbin_tes1]O close\n");
    set_engine_angle_reversal_O(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = FALSE;
}

void Dustbin_open_R(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "              ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_R", 1);
    printf("[Dustbin_tes1]R open\n");
    set_engine_angle_R(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = TRUE;
}
void Dustbin_open_U(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "              ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_U", 1);
    printf("[Dustbin_tes1]U open\n");
    set_engine_angle_U(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = TRUE;
}
void Dustbin_open_K(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "              ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_K", 1);
    printf("[Dustbin_tes1]K open\n");
    set_engine_angle_K(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = TRUE;
}
void Dustbin_open_O(void)
{
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "              ", 1);
    OledShowStr(0, Y_SHOW_SMART_CAN_SHOW_OPEN, "Dustbin:open_O", 1);
    printf("[Dustbin_tes1]O open\n");
    set_engine_angle_O(THE_ANGLE_OF_dustbin_WROK);
    g_dustbinIsOpen = TRUE;
}

int CalcCapacityUsedPercentage(int* capacityUsed)
{
    int data=0;
    printf("[Dustbin_tes1]Current g_Capacityvalue = %f \n",  g_Capacityvalue);

    if ((g_Capacityvalue < 3.3) && (g_Capacityvalue > 3.4))  { // 使用率 100%
        *capacityUsed = 100;
        return 0;
    } else if ((g_Capacityvalue < 0.2) && (g_Capacityvalue > 3.7)) { // 抛弃无意义的数值读取
        printf("capacityUsed: %d\n",&capacityUsed);
        printf("[Dustbin_tes1]the value is invalid_1\n");
        return -1;
    }
    *capacityUsed = ((3.65-g_Capacityvalue) / 0.30) * 100; // 计算公式
    data=(int)*capacityUsed;
    printf("capacityUsed: %d\n",&data);
    if ((data < 0) || (data > 100)) {
        printf("[Dustbin_tes1]the value is invalid_2\n");
        return -1;
    }
    return 0;
}

void SmartdustbinInit(void)
{
    // 舵机初始化 GPIO10
    GpioInitOperation(HAL_WIFI_IOT_IO_NAME_GPIO_10, 0, IOT_GPIO_DIR_OUT, IOT_GPIO_VALUE0);
    GpioInitOperation(HAL_WIFI_IOT_IO_NAME_GPIO_9, 0, IOT_GPIO_DIR_OUT, IOT_GPIO_VALUE0);
    GpioInitOperation(HAL_WIFI_IOT_IO_NAME_GPIO_7, 0, IOT_GPIO_DIR_OUT, IOT_GPIO_VALUE0);
    GpioInitOperation(HAL_WIFI_IOT_IO_NAME_GPIO_6, 0, IOT_GPIO_DIR_OUT, IOT_GPIO_VALUE0);
    
    // 绿色LED灯初始化 GPIO8
    GpioInitOperation(HAL_WIFI_IOT_IO_NAME_GPIO_8, 0, IOT_GPIO_DIR_OUT, IOT_GPIO_VALUE0);
}

void dustbinDistanceDetection(void)
{
    if (is_someone_there() == TRUE) {//判断是否有人靠近
        printf("[Dustbin_tes1][test] someone here \n");
        if (is_night_time() == TRUE) {//判断是否夜间
            printf("[Dustbin_tes1]is night\n");
            printf("[Dustbin_tes1]===Open led===\n");
            IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_8, IOT_GPIO_VALUE1);//设置gpio8输出1
            osDelay(300);
        }
     
    } else if (is_someone_there() == FALSE) {//判断是否有人靠近
            printf("[Dustbin_tes1]===close led===\n");
            IoTGpioSetOutputVal(HAL_WIFI_IOT_IO_NAME_GPIO_8, IOT_GPIO_VALUE0);//gpio8输出0
        }
    
}

void dustbinStatusReporting(void)
{
    if ((g_dustbinIsOpen == FALSE) && (is_Dustbin_full() == TRUE)) {
        printf("[Dustbin_tes1]publish success : please deal with it in time \n");
        OledShowStr(0, Y_SHOW_SMART_CAN_STATUS, "              ", 1);
        OledShowStr(0, Y_SHOW_SMART_CAN_STATUS, "Dustbin:IsFull", 1);
    }
    if ((g_dustbinIsOpen == FALSE) && (is_Dustbin_full() == FALSE)) {
        printf("[Dustbin_tes1]publish success : the trash can is not full \n");
        OledShowStr(0, Y_SHOW_SMART_CAN_STATUS, "              ", 1);
        OledShowStr(0, Y_SHOW_SMART_CAN_STATUS, "Dustbin:NoFull", 1);
    }
}

void *SmartdustbinTask(void *param)
{
    CommonDeviceInit(); // Init OLED device
    SmartdustbinInit();
    while (1) {
        dustbinDistanceDetection();
        dustbinStatusReporting();
        
        hi_sleep(SMART_CAN_SLEEP_2500MS);
    }
}

void SmartdustbinTaskEntry(void)
{
    osThreadAttr_t attr = {0};
    attr.name = (char*)"dustbinTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = TRASH_DEMO_TASK_STACK_SIZE;
    attr.priority = TRASH_DEMO_TASK_PRIOPITY;
    if (osThreadNew((osThreadFunc_t)SmartdustbinTask, NULL, &attr) == NULL) {
        printf("[Dustbin_tes1]Failed to create dustbin test !\n");
    }
}