# Hi3861 通过UART串口协议与其它开发板进行通信

UART（Universal Asynchronous Receiver/Transmitter，通用异步收发器），这是用于全双工串行通信的最常见协议。该设备将数据从一个设备发送到另一个设备。

## 一、搭建编译环境

##### 1、 需要下载虚拟机VMware和Ubuntu20.0.14

-  [下载 VMware Workstation Pro | CN](https://gitee.com/link?target=https%3A%2F%2Fwww.vmware.com%2Fcn%2Fproducts%2Fworkstation-pro%2Fworkstation-pro-evaluation.html)
-  [Ubuntu系统下载 | Ubuntu](https://gitee.com/link?target=https%3A%2F%2Fcn.ubuntu.com%2Fdownload)

##### 2、 安装vm完成后打开vm，点击创建新的虚拟机

![new_vmware](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/new_vmware.png)

##### 3、选择**典型**点击**下一步**，选择下载的Ubuntu，点击下一步，创建完成后，虚拟机会自动安装Ubuntu

![choose_Ubuntu](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/choose_Ubuntu.png)

##### 4、安装完成后，获取Openharmony源码可参考

 链接：[获取源码-入门-HarmonyOS设备开发](https://gitee.com/link?target=https%3A%2F%2Fdevice.harmonyos.com%2Fcn%2Fdocs%2Fdocumentation%2Fguide%2Fsourcecode-acquire-0000001050769927)

##### 5、安装编译工具

###### （1）安装Node.js

打开Ubuntu终端输入命令安装：

```
sudo apt-get install nodejs
sudo apt-get install npm
node --version   //查看nodejs版本
npm --version    //查看npm版本
```

###### （2）安装Python编译环境

```
sudo apt-get install python3.8
sudo apt-get install python3-pip
sudo pip3 install setuptools
sudo pip3 install kconfiglib 
sudo pip3 install pycryptodome
sudo pip3 install six --upgrade --ignore-installed six
sudo pip3 install ecdsa
```

###### （3） 安装SCons

```
python3 -m pip install scons

scons -v   //查看版本
```

如图：![scons_V](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/scons_V.png)

###### （4）安装hb工具 代码测试

```
python3 -m pip install --user ohos-build

vim ~/.bashrc              				//设置环境变量

export PATH=~/.local/bin:$PATH    		//将以下命令拷贝到.bashrc文件的最后一行，保存并退出

source ~/.bashrc						//更新环境变量
```

 执行"hb -h"，有打印以下信息即表示安装成功.

![hb](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/hb.png)

###### （5） 安装gcc_riscv32

下载[gcc_riscv32](https://gitee.com/link?target=https%3A%2F%2Frepo.huaweicloud.com%2Fharmonyos%2Fcompiler%2Fgcc_riscv32%2F7.3.0%2Flinux%2Fgcc_riscv32-linux-7.3.0.tar.gz)镜像

设置环境变量

将压缩包解压到根目录

```
tar -xvf gcc_riscv32-linux-7.3.0.tar.gz -C ~    //文件名需要与下载的文件相匹配
```

设置环境变量。

```
vim ~/.bashrc              				//设置环境变量

export PATH=~/gcc_riscv32/bin:$PATH		//将以下命令拷贝到.bashrc文件的最后一行，保存并退出

source ~/.bashrc						//更新环境变量

riscv32-unknown-elf-gcc -v				//显示版本号，则安装成功
```

##### 6、修改usr_config.mk 文件

文件在Open Harmony源码目录下device/hisilicon/hispark_pegasus/sdk_liteos/build/config/usr_config.mk

```
CONFIG_I2C_SUPPORT=y
CONFIG_PWM_SUPPORT=y
```

##### 7、修改wifiservice 文件夹

文件在Open Harmony源码目录下device/hisilicon/hispark_pegasus/hi3861_adapter/hals/communication/wifi_lite/wifiservice/source/wifi_hotspot.c

```
EnableHotspot函数中屏蔽如下字段
     //if (SetHotspotIpConfig() != WIFI_SUCCESS) {
     //    return ERROR_WIFI_UNKNOWN;
     //}
```

地址：device/hisilicon/hispark_pegasus/hi3861_adapter/hals/communication/wifi_lite/wifiservice/source/wifi_device.c

```
DispatchConnectEvent函数下 屏蔽StaSetWifiNetConfig相关代码行
      //StaSetWifiNetConfig(HI_WIFI_EVT_CONNECTED);
      //StaSetWifiNetConfig(HI_WIFI_EVT_DISCONNECTED);
```



## 二、创建项目文件夹

### 1、在Openharmony1.01版本创建一个项目demo

在源码目录下的vendor/team_x创建smart_demo

在scr里面添加我们写的代码：

![uar1](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/uart/uart1.png)

#### 初始化uart串口

首先我们需要创建一个初始化uart串口的程序

将GPIO0初始化为tx端，将GPIO1初始化为rx端



```c
void UartInit(void){
    RaiseLog(LOG_LEVEL_INFO,"[2022012x01] entry into UartInit");
    IoTGpioInit(HAL_WIFI_IOT_IO_NAME_GPIO_0);
    HalIoSetFunc(HAL_WIFI_IOT_IO_NAME_GPIO_0, WIFI_IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoTGpioInit(HAL_WIFI_IOT_IO_NAME_GPIO_1);
    HalIoSetFunc(HAL_WIFI_IOT_IO_NAME_GPIO_1, WIFI_IOT_IO_FUNC_GPIO_1_UART1_RXD);
    
    hi_uart_attribute uart_attr = {
        .baud_rate = UART_BAUD_RATE, 	  /* baud_rate: 9600 */
        .data_bits = UART_DATA_BITS,      /* data_bits: 8bits */
        .stop_bits = UART_STOP_BITS,
        .parity = 0,
    };

    RaiseLog(LOG_LEVEL_INFO,"[2022012x01] uart_init success");
    /* Initialize uart driver */
    hi_u32 ret = hi_uart_init(HI_UART_IDX_1, &uart_attr, HI_NULL);
    if (ret != HI_ERR_SUCCESS)
    {
        printf("[Dustbin_tes3]Failed to init uart! Err code = %d\n", ret);
        return;
    }
}
```

#### 创建线程任务

```c
static void *uart_demo_task(void)
{
    static uint16_t countSendTimes = 0;
    static uint8_t countReceiveTimes = 0;
    uartController.isReadBusy = false;
    printf("[Initialize uart successfully\n");
    UartInit();
    while (1)
    {
        osDelay(50); 
        UartReceiveMessage();//Collecting Serial Port Data
        hi_sleep(SMART_BIN_SLEEP_2500MS);  
    }
    return 0;
}
static void IotMainEntry(void)
{
    osThreadAttr_t attr;
    RaiseLog(LOG_LEVEL_INFO, "DATA:%s Time:%s \r\n", __FUNCTION__, __DATE__, __TIME__);

    // Create the IoT Main task
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL
    attr.stack_size = CONFIG_TASK_MAIN_STACKSIZE;
    attr.priority = CONFIG_TASK_MAIN_PRIOR;
    attr.name = "IoTMain";
    (void) osThreadNew((osThreadFunc_t)uart_demo_task, NULL, (const osThreadAttr_t *)&attr);
    return;

}
APP_FEATURE_INIT(IotMainEntry);
```



#### 接收串口数据

```c
static void UartReceiveMessage(void)
{
    char *recData;
    printf("----Listening----\n");
    RaiseLog(LOG_LEVEL_INFO,"Start Listening serial port");
    if (UartIsBufEmpty())
        {
            return;
        }
        if (uartController.isReadBusy)
        {
            return;
        }
        uartController.isReadBusy = true;
        g_ReceivedDatalen = hi_uart_read(UART_NUM, g_uart_buff, UART_BUFF_SIZE);
        if (g_ReceivedDatalen > 0)
        {
            printf("handleUartReceiveMessage rcvData len:%d,msg:%s.\n", g_ReceivedDatalen, g_uart_buff);     
            setVoiceCommand();//Setting voice Commands
            memset(g_uart_buff, 0, sizeof(g_uart_buff));
            g_ReceivedDatalen = 0;
        }
        uartController.isReadBusy = false;
}
```



当hi3861开发板接收到其他开发板传输的数据后，可以通过串口打印出来，对此可以写一个解析命令，对发送的字符串，执行相应的命令

![uar2](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/uart/uart2.png)

注：在接线时，要将tx与另一个开发板的rx连接，要交叉连接；tx—RX、TX—rx



![uart3](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/uart/uart3.png)