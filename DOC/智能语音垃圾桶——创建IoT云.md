# 智能语音垃圾桶——创建IoT云



#### 1、 创建lot云（准备阶段）

进入到华为IoTDA界面，登陆网址[链接](https://gitee.com/link?target=https%3A%2F%2Fwww.huaweicloud.com%2Fproduct%2Fiothub.html)。

（1）产品：

![produce_name](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/produce_name.png)

（2）设备列表：

	创建设备ID和验证码后面步骤需要使用

![dev_name](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/dev_name.png)

（4）云通信模块

![iot_api](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/iot_api.png)

 

#### 2、 产品模型

配置产品属性SmartDustbin_DATA

##### 1） 设备状态（Dev_Status）

![dev_status](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/dev_status.png)

##### 2） 可回收垃圾桶内容量（其它三个垃圾桶属性一致）

![RC](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/RC.png)



##### 3）4个垃圾桶的设备命令

![set_command](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/set_command.png)

#### 3、 利用mqtt.fx软件进行测试

##### 1） 下载安装完成后，打开mqtt.fx

![mqtt1](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt1.png)

点击设置按钮，并配置下列信息

![mqtt2](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt2.png)

生成域名：在Windows中cmd中输入ping 设备接入地址（单击“立即使用”进入控制台，单击左侧导航栏的“总览”，查看设备接入信息，记录域名和端口）

![mqtt3](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt3.png)

![mqtt4](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt4.png)

 

点击[这里](https://iot-tool.obs-website.cn-north-4.myhuaweicloud.com/)，访问生成连接信息（ClientId、Username、Password）复制到mqtt中

![mqtt5](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt5.png)

其中的设备ID（DeviceId）和密钥（DeviceSecret）是创建设备所设置的

填写成功后点击apply按钮

 

##### 2） 连接华为iot云

点击connect

![mqtt6](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt6.png)

连接成功后在下面窗口填写topics

![mqtt7](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt7.png)

设备通过MQTT通道上报数据，需要发给指定的topic，上报消息的topic格式（可在创建的产品内查看）为：“$oc/devices/{device_id}/sys/properties/report”，其中**“deviceId”**的值，对一机一密设备，使用deviceId接入时填写为设备注册成功后返回的“deviceId”值。

填写上报数据

![mqtt8](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt8.png)

上报设备信息如下：

```c
{"services": [{"service_id": "SmartDustbin_DATA","properties": {"Dev_Status": 1,"Recyclables_Capacity":20,"Unrecyclable_Capacity":21,"KitchenGarbage_Capacity":13,"OtherRubbish_Capacity":22}}]}
```

 

![mqtt10](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt10.png)

![mqtt11](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt11.png)

 

点击Pubish

![mqtt12](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt12.png)

上传数据后在iot云显示的数据

![mqtt13](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/mqtt13.png)

####  4、创建数字管家服务器

创建自己的服务器可以更好的查看数据和检测数字管家的设备接口

建议使用华为云服务器（可以免费试用30天）

创建成功后如图：

![server](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/server.png)

##### 1)、下载Xshell（用于终端操作）和Xfile（用于传输文件）http://www.netsarang.com/download/software.html

##### 2)、在Xshell登录后创建一个harmony文件夹

```
mkdir harmony
```

##### 3)、从gitee仓库获取数字管家到本地计算机，利用xfile上传数字管家到harmony

##### 4)、解压数字管家

```
apt-get update
apt-get upgrade
apt install unzip 									//安装解压工具

unzip knowledge_demo_smart_home-master.zip 			//就压数字管家
```

##### 5)、安装MySQL和jdk、maven

```
sudo apt install mysql-server
sudo apt-get install openjdk-8-jdk
apt install maven
sudo apt-get install erlang-nox

mysql -v											//查看版本
```

##### 6)、修改MySQL root密码

```
cat /etc/mysql/debian.cnf

# Automatically generated for Debian scripts. DO NOT TOUCH!
[client]
host     = localhost
user     = debian-sys-maint
password = WG3xXWfOZ1usv2Mz
socket   = /var/run/mysqld/mysqld.sock
[mysql_upgrade]
host     = localhost
user     = debian-sys-maint
password = WG3xXWfOZ1usv2Mz
socket   = /var/run/mysqld/mysqld.sock

#找到密码后，用debian-sys-maint登录
mysql -u debian-sys-maint -pWG3xXWfOZ1usv2Mz
#执行下面命令

use mysql;
# 修改用户‘root’的密码
update user set authentication_string=password('123456') where user='root' and Host ='localhost';
# 修改 user 表中的 plugin 类型为本地密码
update user set plugin="mysql_native_password";
flush privileges;
# 退出mysql
\q

#重启服务
service mysql restart

```

##### 7)、安装rabbitmq

```
sudo apt-get install erlang-nox
sudo rabbitmq-plugins enable rabbitmq_management	//安装rabbitmq
sudo rabbitmqctl list_users							//查看用户列表


sudo rabbitmqctl set_user_tags admin administrator	//添加管理员admin账户
sudo rabbitmqctl add_user admin StrongPassword		//设置账户密码为StrongPassword
sudo service rabbitmq-server restart				//重启服务
```

##### 8)、服务器端口开放

点击配网规则

![netconfig](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/netconfig.png)

在**入方向规则**和**出方向规则**加入8080端口和15672端口

![server2](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/server2.png)









#### 5、网页测试

swagger:	 http://xx.xx.xx.xx:8080/distschedule-api/swagger-ui/index.html#/

rabbitmq:	http://xx.xx.xx.xx:15672















参考文档：[dev/docs/iot_huawei/README.md · OpenHarmony-SIG/knowledge_demo_smart_home - Gitee.com](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/dev/docs/iot_huawei/README.md#设备连接iot云平台指南)