# 智能语音垃圾桶 Profile定义



#### IoT下发命令：

```java
 {
	"service_id":"SmartDustbin_DATA",   // 产品服务ID，固定
     
	"command_name":"Open_R", 			// 开关命令名字，固定
	"paras":{
		"OpenRecyclables":"ON",         // 可回收命令参数,选项为ON（打开）/OFF(关闭)
	}
     "command_name":"Open_U", 			// 开关命令名字，固定
	"paras":{
		"OpenUnrecyclable":"ON",        // 不可回收命令参数,选项为ON（打开）/OFF(关闭)
	}
     "command_name":"Open_K", 			// 开关命令名字，固定
	"paras":{
		"OpenKitchenGarbage":"ON",      // 厨余命令参数,选项为ON（打开）/OFF(关闭)
	}
     "command_name":"Open_O", 			// 开关命令名字，固定
	"paras":{
		"OpenOtherRubbish":"ON",        // 其他垃圾命令参数,选项为ON（打开）/OFF(关闭)
	}
}
```



#### 上报垃圾桶属性和状态：

```java
{
	"services": [{"service_id": "SmartDustbin_DATA",
                  "properties": {
                      "Dev_Status":1,					//设备状态1表示在线，0表示离线
                      "Recyclables_Capacity":20,		//可回收垃圾容量有效值（1-100）
                      "Unrecyclable_Capacity":21,		//不可回收垃圾容量有效值（1-100）
                      "KitchenGarbage_Capacity":13,		//厨余垃圾容量有效值（1-100）
                      "OtherRubbish_Capacity":22		//其他垃圾容量有效值（1-100）
                  }
                 }
                ]
}
```

####  产品的其他信息定义

智能风扇设备相关信息，用于写入NFC自定义数据之中

| 标签 | 长度 | 名字                | 描述                                                         | 定义值                   |
| ---- | ---- | ------------------- | ------------------------------------------------------------ | ------------------------ |
| 1    | 24   | IoTDA平台设备产品ID | 由IoTDA平台中获取，使用自身，标识设备产品品类。典型值24字节，小于32字节 | 61d2add4a61a2a029ccbe02d |
| 2    | 15   | NodeID              | 设备节点ID，辨识同品类中，不同的设备。典型值8字节，小于64字节 | SmartDustbin001          |
| 3    | 08   | DevicePwd           | 设备认证秘钥，用于设备连接IoTDA认证。典型值8字节，小于32字节 | 12345678                 |
| 4    | 01   | 配网标识            | 标识当前设备配网类型，1字节 0：不需要配网设备（如手表平板等自带蜂窝网络的设备）； 1：NAN配网 + softAP组合模式配网（能自动使用当前网络配网，不需要输入密码）; 2：softAP配网，连入设备热点，输入wifi密码进行配网; 3：ble蓝牙配网; 4：NAN配网 近距离贴近设备配网（能自动使用当前网络配网，不需要输入密码）； | 1                        |
| 5    | 15   | ApSSID              | 设备自身热点名，典型值12字节，小于32字节，NAN配网和softAp配网必须提供；构成一般为前缀teamX + nodeID | teamX-Dustbin01          |

生成自定义标签：12461d2add4a61a2a029ccbe02d215SmartDustbin001308123456784011515teamX-Dustbin01

