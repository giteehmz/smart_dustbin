# Hi3861开发板 利用pwm控制舵机

## 关于pwm的函数：
`unsigned int PwmInit(wifiIotPwmPort port);` //pwm模块初始化
`unsigned int PwmStart(WifiIotPwmPort port,unsigned short duty,unsigned short freq);`
开始输出pwm信号，
WifiIotPwmPort port：pwm的输出口，
freq控制输出的pwm信号频率，值为时钟源频率Fclk除以freq，Fout=Fclk/freq；freq最大为65600。
duty，指定占空比，占空比=duty/freq；当duty为65000时，为0度，当duty为64500时，为80度左右，当duty为60000时，为145度左右。
`unsigned int PwmStop(wifiIotPwmPort port);`//停止输出pwm信号；
`unsigned int PwmDeinit(wifiIotPwmPort port);`//解除排气门模块的初始化；
`unsigned int PwmSetClock(WifiIotPwmClkSource clkSource);`//设置pwm模块时钟源；

## 1.创建pwm_buz.c文件
在openharmony源代码的wifi_iot/app/目录下创建led_demo的目录，在这个目录下创建led.c和BUILD.gn的文件。

```
#include <stdio.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "wifiiot_gpio.h"
#include "wifiiot_gpio_ex.h"
#include "wifiiot_pwm.h"

static void PwmBuzTask(void *arg)
{
    (void) arg;
    GpioInit();//GPIO口初始化
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_1,WIFI_IOT_IO_FUNC_GPIO_1_PWM4_OUT);
    //将gpio1口设置为pwm输出口，输出pwm4信号。
    PwmInit(WIFI_IOT_PWM_PORT_PWM4);//pwm初始化
    while(1)
    {
        PwmStart(WIFI_IOT_PWM_PORT_PWM4,65000,65400);    //0度
        osDelay(400);//延时4s
        PwmStart(WIFI_IOT_PWM_PORT_PWM4,64500,65400);    //80度左右
        osDelay(400);
    }
}

static void PwmBuzEntry(void) //几乎不变，只修改attr.name和SYS_RUN就行了
{
    osThreadAttr_t attr = {0};
    attr.name = "PwmBuzTask";
    attr.stack_size = 4096;
    attr.priority = osPriorityNormal;
    if(osThreadNew(PwmBuzTask,NULL,&attr) == NULL)
    {
        printf("[PwmBuzEntry] create PwmBuzTask failed!\n");
    }
}
SYS_RUN(PwmBuzEntry);
```




## 2.创建BUILD.gn文件
```
static_library("pwm_demo")
{
    sources = ["pwm_buz.c"]

    include_dirs = [
        "//third_party/cmsis/CMSIS/RTOS2/Include",
        "//utils/native/lite/include",
        "//base/iot_hardware/interfaces/kits/wifiiot_lite",
    ]
}
```

修改applications/sampie/wifi-iot/app目录下的BUILD.gn文件，将其中的features值修改为pwm_demo.```
    features = [
        "iothardware:pwm_demo",
    ]
```

**修改vendor/hisi/hi3861/hi3861/build/config/usr_config.mk文件，将其中的#CONFIG_PWM_SUPPORT is not set 改为CONFIG_PWM_SUPPORT=y.**

编译

烧录