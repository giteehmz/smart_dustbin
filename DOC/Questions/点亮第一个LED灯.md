# 点亮第一个led灯

因为之前下载的源码是3.0版本的，在运行的时候函数不正确，错误弄得我老壳发涨，可能版本之间的跨度太大了，我还没有水平去解决，所以，在ubuntu里面，我在以前的基础上，把1.0版本的编译环境又安装了一遍。

## 一、编译所需工具

使用如下apt-get命令安装编译所需的必要的库和工具:
sudo apt-get install build-essential gcc g++ make zlib* libffi-dev e2fsprogs pkg-config flex bison perl bc openssl libssl-dev libelf-dev libc6-dev-amd64 binutils binutils-dev libdwarf-dev u-boot-tools mtd-utils gcc-arm-linux-gnueabi cpio device-tree-compiler

安装python包管理工具：
sudo apt install python3-pip

安装scons软件包
Pip3 install scons

安装kconfiglib：
Pip3 install kconfiglib

安装pycryptodome和ecdsa
Pip3 install pycryptodome

下载code-1.0.tar.gz

编写代码：
在映射盘里面，直接把解压后的源码拖入vscode里面打开，就可以直接编写代码了，编写完成后保存，在MobaXterm里面编译，我觉得这样比较方便点，不用频繁的打开虚拟机。

在openharmony源代码的wifi_iot/app/目录下创建led_demo的目录，在这个目录下创建led.c和BUILD.gn的文件。
在led.c里面输入

```c
#include <stdio.h>
#include "ohos_init.h"//在utils\native\lite\include中
#include "cmsis_os2.h"//在third_party\cmsis\CMSIS\RTOS2\Include中
#include "wifiiot_gpio.h"//在base\iot_hardware\frameworks\wifiiot_lite\src中
#include "wifiiot_gpio_ex.h"//同上

static void LedTask(void *arg)
{
    (void) arg;
    GpioInit();//初始化
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_9,WIFI_IOT_IO_FUNC_GPIO_9_GPIO);
    GpioSetDir(WIFI_IOT_GPIO_IDX_9,WIFI_IOT_GPIO_DIR_OUT);//让gpio9输出电平

    while (1)
    {
        GpioSetOutputVal(WIFI_IOT_GPIO_IDX_9,0);//让gpio9输出高电平
        osDelay(50);//延时
        GpioSetOutputVal(WIFI_IOT_GPIO_IDX_9,1);//让gpio9输出低电平
        osDelay(50);//延时
        /* code */
    }

}
static void LedEntry(void)
{
    osThreadAttr_t attr = {0};

    attr.name = "LedTask";
    attr.stack_size = 4096;
    attr.priority = osPriorityAboveNormal1;
    
    if(osThreadNew(LedTask,NULL,&attr) == NULL)
    {
        printf("[LedEntry] creat LedTask failed!\n");
    }

}
SYS_RUN(LedEntry);
```



在BUILD.gn里面输入：
static_library("led_demo")
{
    sources = ["led.c"]

    include_dirs = [
        "//third_party/cmsis/CMSIS/RTOS2/Include",//这些都是头文件的相对路径
        "//utils/native/lite/include",
        "//base/iot_hardware/interfaces/kits/wifiiot_lite",
    ]
}
打开wifi_iot/app目录下的BUILD.gn，将里面的“startup”换成“led_demo”。
在源代码的顶层目录下执行python build.py wifiiot 
最后烧录就可以运行了。