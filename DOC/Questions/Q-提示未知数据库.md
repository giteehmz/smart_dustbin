# 提示未知数据库

![que1](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/question/que1.png)

解决方法： 

#### 1） 先确定虚拟机中mysql -uoot -p能连接上

#### 2） netstat -apn|grep 3306 ，如果显示的是127.0.0.1:3306

#### 3） 在终端输入sudo vim /etc/mysql/mysql.conf.d/mysqld.cnf  在vim中注释掉bind-address如图

![que2](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/question/que2.png)



#### 4） 重启mysql ： service mysql restart

#### 5） netstat -apn|grep 3306

![que3](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/question/que3.png)



 

#### 6） 在虚拟机修改远程登录的权限先进入MySQL ：use msyql;update user set host='%' where user='root'；之后刷新权限flush privileges;

#### 7） 在navicat中创建MySQL连接，主机ip在Ubuntu中使用命令ifconfig查看

![que4](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/question/que4.png)



#### 8）连接数据库成功后，点击新建数据库

![que5](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/question/que5.png)

![image-20220217165809661](E:/OpenHarmony_Project/Smart_Dustbin/Media/question/que5.png

#### 9)新建数据库配置如下

![que6](https://gitee.com/giteehmz/smart_dustbin/raw/master/Media/question/que6.png)

